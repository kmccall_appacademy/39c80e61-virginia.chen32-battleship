class HumanPlayer

  def initialize(name)
    @name=name
  end

  def get_play
    puts "What's your guess? Format: 0,1"
    guessed_code = gets.chomp
        a = guessed_code.split(",")
        a[0] = a[0].to_i
        a[1] = a[1].to_i
        a
  end
end
