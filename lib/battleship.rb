require_relative 'board'
require_relative 'player'

class BattleshipGame

  def initialize(player=HumanPlayer.new("Sam"),board=Board.new)
    @board = board
    @player = player
  end

  attr_reader :board, :player

  def play
    80.times { board.place_random_ship }
    until game_over?
      play_turn
    end
  end

  def play_turn
    guess = @player.get_play
    if board.empty?(guess) || guess == nil
      puts "You missed!"
    elsif board.grid[guess[0]][guess[1]] == :o
      puts "You've already made this guess"
    else puts "You have a hit!"
    end
    self.attack(guess)
  end

  def attack(loc)
    @board.grid[loc[0]][loc[1]]=:x
  end

  def game_over?
    board.won?
  end

  def count
    board.count
  end

end

if __FILE__ == $PROGRAM_NAME
  game = BattleshipGame.new
  game.play
end
