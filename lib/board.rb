class Board

  def initialize(grid = Board.default_grid)
    @grid = grid
  end

  attr_accessor :grid

  def self.default_grid
    grid = [
        [nil, nil, nil, nil, nil, nil, nil, nil, nil, nil],
        [nil, nil, nil, nil, nil, nil, nil, nil, nil, nil],
        [nil, nil, nil, nil, nil, nil, nil, nil, nil, nil],
        [nil, nil, nil, nil, nil, nil, nil, nil, nil, nil],
        [nil, nil, nil, nil, nil, nil, nil, nil, nil, nil],
        [nil, nil, nil, nil, nil, nil, nil, nil, nil, nil],
        [nil, nil, nil, nil, nil, nil, nil, nil, nil, nil],
        [nil, nil, nil, nil, nil, nil, nil, nil, nil, nil],
        [nil, nil, nil, nil, nil, nil, nil, nil, nil, nil],
        [nil, nil, nil, nil, nil, nil, nil, nil, nil, nil],
        ]
  end

  def display
    self.grid.each do |row|
      puts "#{row}"
    end
  end

  def count
    count = 0
    self.grid.each do |row|
      count+=row.count(:s)
    end
    count
  end

  def [](pos)
    row, col = pos
    @grid[row][col]
  end

  def []=(pos, mark)
    row, col = pos
    @grid[row][col] = mark
  end

  def empty?(position = nil)
    if position == nil
      if self.count == 0
        return true
      else return false
      end
    else
      return true if self.grid[position[0]][position[1]] == nil
    end
    return false
  end

  def full?
    self.grid.each_with_index do |row,idx1|
      self.grid.each_with_index do |el,idx2|
        return false if grid[idx1][idx2] != :s
      end
    end
    return true
  end

  def won?
    self.grid.each_with_index do |row,idx1|
      self.grid.each_with_index do |el,idx2|
        return false if grid[idx1][idx2] == :s
      end
    end
    return true
  end

  def place_random_ship
    if self.full?
      raise Exception.new("Board is full!")
    else
      @grid[rand(grid.length)][rand(grid.length)] = :s  
    end
  end

end
